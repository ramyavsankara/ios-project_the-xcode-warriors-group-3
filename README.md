# Details #
Name of Application: Fun to Learn!

This is a team project for the iOS app development class(Section 1)
Instructor- Dr. Micheal Rogers
Course Assistant- Sahithya Pasnoor

This repository has the application mentioned above-
It is a fun and educative app for toddlers to help them learn letters, numbers, colors and shapes.

* Quick summary 
* Version - Alpha 1.1
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

# Setup #
* Summary of set up
1. clone the repo in your system
2. Run it using xcode
* Configuration
1. iOS 9.2
* Dependencies
1. Xcode 2.x required 
2. works on iOS devices like iPhones,iPads only

--Coming shortly--
* Few additional features in play scene
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines
# References #
* Images - Google open source
* Soundes - open source and other free sources

# Contact #
* Repo owner or admin
* 1. ramyavsankara
* 2. srujanmathur
* 3. sama sainath reddy
* Other community or team contact