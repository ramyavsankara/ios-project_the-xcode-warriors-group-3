//
//  GameScene.swift
//  Fun to Learn!!
//
//  Created by Sankara,Ramya Virajitha on 3/14/16.
//  Copyright (c) 2016 Sankara,Ramya Virajitha. All rights reserved.
//

import SpriteKit
//Home scene is rendered here
class GameScene: SKScene {
    //nodes are added to screen
    let bgImage = SKSpriteNode(imageNamed: "bg-intro.png")
    let logo = SKSpriteNode(imageNamed: "game_logo.png")
    let play = SKSpriteNode(imageNamed: "play.png")
    let alphabet = SKSpriteNode(imageNamed: "abc_.png")
    let number = SKSpriteNode(imageNamed: "123_.png")
    let shape = SKSpriteNode(imageNamed: "shape_.png")
    let color = SKSpriteNode(imageNamed: "color_.png")
    let settings = SKSpriteNode(imageNamed: "music_ON.png")
   // var soundAction = SKAction.playSoundFileNamed("background.mp3", waitForCompletion: true);
    var backgroundMusic: SKAudioNode!
    var label1: SKLabelNode!
    var label2: SKLabelNode!
    var label3: SKLabelNode!
    var label4: SKLabelNode!
    

    
    override func didMoveToView(view: SKView) {
        
        /* Setup your scene here */
       
        // self.runAction(soundAction, withKey:"die-sound");
        bgImage.position = CGPointMake(self.size.width/2, self.size.height/2)
        logo.size = CGSize(width:600,height:500)
        logo.position = CGPoint(x:CGRectGetMidX(self.frame),y: 550)
        play.position = CGPoint(x: CGRectGetMidX(self.frame), y: CGRectGetMidY(self.frame))
        play.size = CGSize(width:100,height:100)
        //  play.position = CGPoint(x:CGRectGetMidX(self.frame),y:CGRectGetMidY(self.frame))
        play.name = "play"
        play.userInteractionEnabled = false
        alphabet.position = CGPoint(x: CGRectGetMidX(self.frame)-300, y: CGRectGetMidY(self.frame)+120)
        alphabet.size = CGSize(width:390,height:300)
        alphabet.name = "abc"
        number.position = CGPoint(x: CGRectGetMidX(self.frame)+300, y: CGRectGetMidY(self.frame)+150)
        number.size = CGSize(width:350,height:250)
        number.name = "123"
        shape.position = CGPoint(x: CGRectGetMidX(self.frame)-300, y: CGRectGetMidY(self.frame)-150)
        shape.size = CGSize(width:400,height:300)
        shape.name = "shape"
        color.position = CGPoint(x: CGRectGetMidX(self.frame)+300, y: CGRectGetMidY(self.frame)-150)
        color.size = CGSize(width:430,height:300)
        color.name = "color"
        settings.name = "settings"
        settings.position = CGPoint(x: CGRectGetMidX(self.frame)+450, y: CGRectGetMidY(self.frame)+350)
        let rotR = SKAction.rotateByAngle(0.10, duration: 0.2)
        let rotL = SKAction.rotateByAngle(-0.1, duration: 0.2)
        let cycle = SKAction.sequence([rotR, rotL, rotL, rotR])
        let wiggle = SKAction.repeatActionForever(cycle)
        play.runAction(wiggle, withKey: "wiggle")
        runAction(SKAction.rotateToAngle(0, duration: 0.2), withKey:"rotate")
        alphabet.runAction(SKAction.rotateByAngle(0.15, duration: 0.0))
        
        number.runAction(SKAction.rotateByAngle(0.25, duration: 0.0))
        
        shape.runAction(SKAction.rotateByAngle(-0.30, duration: 0.0))
        
        color.runAction(SKAction.rotateByAngle(-0.25, duration: 0.0))
        
        label1 = SKLabelNode(fontNamed: "Chalkduster")
        label1.text = "Alphabets"
        label1.fontColor = UIColor.blackColor()
        label1.fontSize = 40
        label1.position = CGPoint(x: CGRectGetMidX(self.frame)-300, y: CGRectGetMidY(self.frame)+250)
        
        label2 = SKLabelNode(fontNamed: "Chalkduster")
        label2.text = "Numbers"
        label2.fontColor = UIColor.blackColor()
        label2.fontSize = 40
        label2.position = CGPoint(x: CGRectGetMidX(self.frame)+300, y: CGRectGetMidY(self.frame)+250)
        
        label3 = SKLabelNode(fontNamed: "Chalkduster")
        label3.text = "Colors"
        label3.fontColor = UIColor.blackColor()
        label3.fontSize = 40
        label3.position = CGPoint(x: CGRectGetMidX(self.frame)+300, y: CGRectGetMidY(self.frame)-60)
        
        label4 = SKLabelNode(fontNamed: "Chalkduster")
        label4.text = "Shapes"
        label4.fontColor = UIColor.blackColor()
        label4.fontSize = 40
        label4.position = CGPoint(x: CGRectGetMidX(self.frame)-300, y: CGRectGetMidY(self.frame)-60)
        
        bgImage.zPosition = 1
        logo.zPosition = 2
        play.zPosition = 3
        alphabet.zPosition = 4
        number.zPosition = 5
        shape.zPosition = 6
        color.zPosition = 7
        settings.zPosition = 8
        label1.zPosition = 9
        label2.zPosition = 10
        label3.zPosition = 11
        label4.zPosition = 12
        runAction(SKAction.waitForDuration(0.10), completion: {
            self.backgroundMusic = SKAudioNode(fileNamed: "background.mp3")
            self.addChild(self.backgroundMusic)
        })
        self.addChild(bgImage)
        self.addChild(logo)
        self.addChild(play)
        self.addChild(alphabet)
        self.addChild(number)
        self.addChild(shape)
        self.addChild(color)
        self.addChild(settings)
        self.addChild(label1)
        self.addChild(label2)
        self.addChild(label3)
        self.addChild(label4)

       
    }
    // when nodes on screen are touched these actions are performed
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        /* Called when a touch begins */
        
        for touch in touches {
            let location = touch.locationInNode(self)
            
            let touchedNode = self.nodeAtPoint(location)
            
            if let name = touchedNode.name
            {
                //when abc is touched
                if name == "abc"
                {
                    //let transition = SKTransition.revealWithDirection(.Right, duration: 0.1)
                    let transition = SKTransition.crossFadeWithDuration(1.0)
                    let playScene = Alphabets(size: self.size)
                    self.view!.presentScene(playScene, transition: transition)
                    backgroundMusic.runAction(SKAction.stop())

                //when 123 is touched
                }else if name == "123"  {
                    //let transition = SKTransition.revealWithDirection(.Right, duration: 0.1)
                    let transition = SKTransition.crossFadeWithDuration(1.0)
                    let playScene = Numbers(size: self.size)
                    self.view! .presentScene(playScene, transition: transition)
                    backgroundMusic.runAction(SKAction.stop())
                //when colors are touched
                } else if name == "color" {
                    //let transition = SKTransition.revealWithDirection(.Right, duration: 0.1)
                    let transition = SKTransition.crossFadeWithDuration(1.0)
                    let playScene = Colors(size: self.size)
                    self.view! .presentScene(playScene, transition: transition)
                    backgroundMusic.runAction(SKAction.stop())
                  //when shapes are touched
                } else if name == "shape" {
                    //let transition = SKTransition.revealWithDirection(.Right, duration: 0.1)
                    let transition = SKTransition.crossFadeWithDuration(1.0)
                    let playScene = Shapes(size: self.size)
                    self.view! .presentScene(playScene, transition: transition)
                    backgroundMusic.runAction(SKAction.stop())
                    
                }
                    //when play is touched
                else if name == "play"  {
                    //let transition = SKTransition.revealWithDirection(.Right, duration: 0.1)
                    let transition = SKTransition.crossFadeWithDuration(1.0)
                    let playScene = PlayScene(size: self.size)
                    self.view! .presentScene(playScene, transition: transition)
                    backgroundMusic.runAction(SKAction.stop())
                    
                }
                else if name == "settings" {
                    backgroundMusic.runAction(SKAction.stop())
                }

        }
    }
    }
    
    override func update(currentTime: CFTimeInterval) {
        /* Called before each frame is rendered */
    }
}
