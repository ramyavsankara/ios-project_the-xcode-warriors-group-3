//
//  Alphabets.swift
//  Fun to Learn!!
//
//  Created by Gade,Srujan Mathur on 4/6/16.
//  Copyright © 2016 Sankara,Ramya Virajitha. All rights reserved.
//

import SpriteKit
//Renders logic for Alphabets Scene
class Alphabets: SKScene {
    
    let bgImage = SKSpriteNode(imageNamed: "bg-intro.png")
    let back = SKSpriteNode(imageNamed: "bt_Back.png")
    enum Letter {
        case A
        case B
        case C
        case D
        case E
        case F
        case G
        case H
        case I
        case J
        case K
        case L
        case M
        case N
        case O
        case P
        case Q
        case R
        case S
        case T
        case U
        case V
        case W
        case X
        case Y
        case Z
    }
    
    var letter = Letter.A
    
    let currentCardBackground = SKSpriteNode(imageNamed: "card1.png")
    let currentCard = SKSpriteNode(imageNamed: "lbc-1.png")
    let upNextCardBackground = SKSpriteNode(imageNamed: "")
    let upNextCard = SKSpriteNode(imageNamed: "lbc-2.png")
    let previousCard = SKSpriteNode(imageNamed: "lbc-26.png")
    
    let forwardArrow = SKSpriteNode(imageNamed: "bt_Arrow_Forward.png")
    let backwardArrow = SKSpriteNode(imageNamed: "bt_Arrow_Backward.png")
    
    var cardFlipped = false
    //function for removing image from scene
    func subtractLetter() {
        cardFlipped = false
      forwardArrow.name = "Forward"
      backwardArrow.name = "Backward"
      upNextCard.name = "upNextCard"
      previousCard.name = "previousCard"

     switch letter {
      case .B:
         letter = Letter.A
         runAction(SKAction.playSoundFileNamed("1c.mp3", waitForCompletion: false))
         upNextCard.texture = SKTexture(imageNamed: "lbc-2.png")
         currentCard.texture = SKTexture(imageNamed: "lbc-1.png")
         previousCard.texture = SKTexture(imageNamed: "lbc-26.png")
         previousCard.hidden = true
         backwardArrow.hidden = true
      case .C:
         letter = Letter.B
         runAction(SKAction.playSoundFileNamed("2c.mp3", waitForCompletion: false))
         upNextCard.texture = SKTexture(imageNamed: "lbc-3.png")
         currentCard.texture = SKTexture(imageNamed: "lbc-2.png")
         previousCard.texture = SKTexture(imageNamed: "lbc-1.png")
     case .D:
      letter = Letter.C
      runAction(SKAction.playSoundFileNamed("3c.mp3", waitForCompletion: false))
      upNextCard.texture = SKTexture(imageNamed: "lbc-4.png")
      currentCard.texture = SKTexture(imageNamed: "lbc-3.png")
      previousCard.texture = SKTexture(imageNamed: "lbc-2.png")
     case .E:
      letter = Letter.D
      runAction(SKAction.playSoundFileNamed("4c.mp3", waitForCompletion: false))
      upNextCard.texture = SKTexture(imageNamed: "lbc-5.png")
      currentCard.texture = SKTexture(imageNamed: "lbc-4.png")
      previousCard.texture = SKTexture(imageNamed: "lbc-3.png")
     case .F:
         letter = Letter.E
         runAction(SKAction.playSoundFileNamed("5c.mp3", waitForCompletion: false))
         upNextCard.texture = SKTexture(imageNamed: "lbc-6.png")
         currentCard.texture = SKTexture(imageNamed: "lbc-5.png")
         previousCard.texture = SKTexture(imageNamed: "lbc-4.png")
     case .G:
      letter = Letter.F
      runAction(SKAction.playSoundFileNamed("6c.mp3", waitForCompletion: false))
      upNextCard.texture = SKTexture(imageNamed: "lbc-7.png")
      currentCard.texture = SKTexture(imageNamed: "lbc-6.png")
      previousCard.texture = SKTexture(imageNamed: "lbc-5.png")
     case .H:
               letter = Letter.G
         runAction(SKAction.playSoundFileNamed("7c.mp3", waitForCompletion: false))
         upNextCard.texture = SKTexture(imageNamed: "lbc-8.png")
         currentCard.texture = SKTexture(imageNamed: "lbc-7.png")
         previousCard.texture = SKTexture(imageNamed: "lbc-6.png")
         upNextCard.size.width *= 2
     case .I:
         letter = Letter.H
         currentCard.size.width *= 2
         runAction(SKAction.playSoundFileNamed("8c.mp3", waitForCompletion: false))
         upNextCard.texture = SKTexture(imageNamed: "lbc-9.png")
         currentCard.texture = SKTexture(imageNamed: "lbc-8.png")
         previousCard.texture = SKTexture(imageNamed: "lbc-7.png")
         upNextCard.size.width /= 2
     case .J:
         letter = Letter.I
         currentCard.size.width /= 2
         runAction(SKAction.playSoundFileNamed("9c.mp3", waitForCompletion: false))
         upNextCard.texture = SKTexture(imageNamed: "lbc-10.png")
         currentCard.texture = SKTexture(imageNamed: "lbc-9.png")
         previousCard.texture = SKTexture(imageNamed: "lbc-8.png")
         previousCard.size.width *= 2
     case .K:
         letter = Letter.J
         runAction(SKAction.playSoundFileNamed("10c.mp3", waitForCompletion: false))
         upNextCard.texture = SKTexture(imageNamed: "lbc-11.png")
         currentCard.texture = SKTexture(imageNamed: "lbc-10.png")
         previousCard.texture = SKTexture(imageNamed: "lbc-9.png")
         previousCard.size.width /= 2
     case .L:
         letter = Letter.K
         runAction(SKAction.playSoundFileNamed("11c.mp3", waitForCompletion: false))
         upNextCard.texture = SKTexture(imageNamed: "lbc-12.png")
         currentCard.texture = SKTexture(imageNamed: "lbc-11.png")
         previousCard.texture = SKTexture(imageNamed: "lbc-10.png")
     case .M:
         letter = Letter.L
         runAction(SKAction.playSoundFileNamed("12c.mp3", waitForCompletion: false))
         upNextCard.texture = SKTexture(imageNamed: "lbc-13.png")
         currentCard.texture = SKTexture(imageNamed: "lbc-12.png")
         previousCard.texture = SKTexture(imageNamed: "lbc-11.png")
     case .N:
         letter = Letter.M
         runAction(SKAction.playSoundFileNamed("13c.mp3", waitForCompletion: false))
         upNextCard.texture = SKTexture(imageNamed: "lbc-14.png")
         currentCard.texture = SKTexture(imageNamed: "lbc-13.png")
         previousCard.texture = SKTexture(imageNamed: "lbc-12.png")
     case .O:
         letter = Letter.N
         runAction(SKAction.playSoundFileNamed("14c.mp3", waitForCompletion: false))
         upNextCard.texture = SKTexture(imageNamed: "lbc-15.png")
         currentCard.texture = SKTexture(imageNamed: "lbc-14.png")
         previousCard.texture = SKTexture(imageNamed: "lbc-13.png")
     case .P:
         letter = Letter.O
         runAction(SKAction.playSoundFileNamed("15c.mp3", waitForCompletion: false))
         upNextCard.texture = SKTexture(imageNamed: "lbc-16.png")
         currentCard.texture = SKTexture(imageNamed: "lbc-15.png")
         previousCard.texture = SKTexture(imageNamed: "lbc-14.png")
     case .Q:
         letter = Letter.P
         runAction(SKAction.playSoundFileNamed("16c.mp3", waitForCompletion: false))
         upNextCard.texture = SKTexture(imageNamed: "lbc-17.png")
         currentCard.texture = SKTexture(imageNamed: "lbc-16.png")
         previousCard.texture = SKTexture(imageNamed: "lbc-15.png")
     case .R:
         letter = Letter.Q
         runAction(SKAction.playSoundFileNamed("17c.mp3", waitForCompletion: false))
         upNextCard.texture = SKTexture(imageNamed: "lbc-18.png")
         currentCard.texture = SKTexture(imageNamed: "lbc-17.png")
         previousCard.texture = SKTexture(imageNamed: "lbc-16.png")
     case .S:
         letter = Letter.R
         runAction(SKAction.playSoundFileNamed("18c.mp3", waitForCompletion: false))
         upNextCard.texture = SKTexture(imageNamed: "lbc-19.png")
         currentCard.texture = SKTexture(imageNamed: "lbc-18.png")
         previousCard.texture = SKTexture(imageNamed: "lbc-17.png")
     case .T:
         letter = Letter.S
         runAction(SKAction.playSoundFileNamed("19c.mp3", waitForCompletion: false))
         upNextCard.texture = SKTexture(imageNamed: "lbc-20.png")
         currentCard.texture = SKTexture(imageNamed: "lbc-19.png")
         previousCard.texture = SKTexture(imageNamed: "lbc-18.png")
     case .U:
         letter = Letter.T
         runAction(SKAction.playSoundFileNamed("20c.mp3", waitForCompletion: false))
         upNextCard.texture = SKTexture(imageNamed: "lbc-21.png")
         currentCard.texture = SKTexture(imageNamed: "lbc-20.png")
         previousCard.texture = SKTexture(imageNamed: "lbc-19.png")
     case .V:
         letter = Letter.U
         runAction(SKAction.playSoundFileNamed("21c.mp3", waitForCompletion: false))
         upNextCard.texture = SKTexture(imageNamed: "lbc-22.png")
         currentCard.texture = SKTexture(imageNamed: "lbc-21.png")
         previousCard.texture = SKTexture(imageNamed: "lbc-20.png")
     case .W:
         letter = Letter.V
         //runAction(SKAction.playSoundFileNamed("22c.mp3", waitForCompletion: false))
         upNextCard.texture = SKTexture(imageNamed: "lbc-23.png")
         currentCard.texture = SKTexture(imageNamed: "lbc-22.png")
         previousCard.texture = SKTexture(imageNamed: "lbc-21.png")
     case .X:
         letter = Letter.W
         runAction(SKAction.playSoundFileNamed("23c.mp3", waitForCompletion: false))
         upNextCard.texture = SKTexture(imageNamed: "lbc-24.png")
         currentCard.texture = SKTexture(imageNamed: "lbc-23.png")
         previousCard.texture = SKTexture(imageNamed: "lbc-22.png")
     case .Y:
         letter = Letter.X
         runAction(SKAction.playSoundFileNamed("24c.mp3", waitForCompletion: false))
         upNextCard.texture = SKTexture(imageNamed: "lbc-25.png")
         currentCard.texture = SKTexture(imageNamed: "lbc-24.png")
         previousCard.texture = SKTexture(imageNamed: "lbc-23.png")
     case .Z:
         letter = Letter.Y
         runAction(SKAction.playSoundFileNamed("25c.mp3", waitForCompletion: false))
         upNextCard.texture = SKTexture(imageNamed: "lbc-26.png")
         currentCard.texture = SKTexture(imageNamed: "lbc-25.png")
         previousCard.texture = SKTexture(imageNamed: "lbc-24.png")
      

          default : break
      
      }
      
    }
    //function for adding image from scene
    func addLetter() {
        cardFlipped = false
      forwardArrow.name = "Forward"
      backwardArrow.name = "Backward"
      upNextCard.name = "upNextCard"
      previousCard.name = "previousCard"

      switch letter {
         
      
      case .A:
            letter = Letter.B
            runAction(SKAction.playSoundFileNamed("2c.mp3", waitForCompletion: false))
            previousCard.texture = SKTexture(imageNamed: "lbc-1.png")
            currentCard.texture = SKTexture(imageNamed: "lbc-2.png")
            upNextCard.texture = SKTexture(imageNamed: "lbc-3.png")
            previousCard.hidden = false
            backwardArrow.hidden = false
      case .B:
            letter = Letter.C
            runAction(SKAction.playSoundFileNamed("3c.mp3", waitForCompletion: false))
            previousCard.texture = SKTexture(imageNamed: "lbc-2.png")
            currentCard.texture = SKTexture(imageNamed: "lbc-3.png")
            upNextCard.texture = SKTexture(imageNamed: "lbc-4.png")
      case .C:
            letter = Letter.D
            runAction(SKAction.playSoundFileNamed("4c.mp3", waitForCompletion: false))
            previousCard.texture = SKTexture(imageNamed: "lbc-3.png")
            currentCard.texture = SKTexture(imageNamed: "lbc-4.png")
            upNextCard.texture = SKTexture(imageNamed: "lbc-5.png")
      case .D:
            letter = Letter.E
            runAction(SKAction.playSoundFileNamed("5c.mp3", waitForCompletion: false))
            previousCard.texture = SKTexture(imageNamed: "lbc-4.png")
            currentCard.texture = SKTexture(imageNamed: "lbc-5.png")
            upNextCard.texture = SKTexture(imageNamed: "lbc-6.png")
      case .E:
            letter = Letter.F
            runAction(SKAction.playSoundFileNamed("6c.mp3", waitForCompletion: false))
            previousCard.texture = SKTexture(imageNamed: "lbc-5.png")
            currentCard.texture = SKTexture(imageNamed: "lbc-6.png")
            upNextCard.texture = SKTexture(imageNamed: "lbc-7.png")
      case .F:
            letter = Letter.G
            runAction(SKAction.playSoundFileNamed("7c.mp3", waitForCompletion: false))
            previousCard.texture = SKTexture(imageNamed: "lbc-6.png")
            currentCard.texture = SKTexture(imageNamed: "lbc-7.png")
            upNextCard.texture = SKTexture(imageNamed: "lbc-8.png")
      case .G:
            letter = Letter.H
            runAction(SKAction.playSoundFileNamed("8c.mp3", waitForCompletion: false))
            previousCard.texture = SKTexture(imageNamed: "lbc-7.png")
            currentCard.texture = SKTexture(imageNamed: "lbc-8.png")
            upNextCard.texture = SKTexture(imageNamed: "lbc-9.png")
            upNextCard.size.width /= 2
      case .H:
            letter = Letter.I
            currentCard.size.width /= 2
            runAction(SKAction.playSoundFileNamed("9c.mp3", waitForCompletion: false))
            previousCard.texture = SKTexture(imageNamed: "lbc-8.png")
            currentCard.texture = SKTexture(imageNamed: "lbc-9.png")
            upNextCard.texture = SKTexture(imageNamed: "lbc-10.png")
            upNextCard.size.width *= 2
      case .I:
            letter = Letter.J
            currentCard.size.width *= 2
           runAction(SKAction.playSoundFileNamed("10c.mp3", waitForCompletion: false))
            previousCard.texture = SKTexture(imageNamed: "lbc-9.png")
            currentCard.texture = SKTexture(imageNamed: "lbc-10.png")
            upNextCard.texture = SKTexture(imageNamed: "lbc-11.png")
            previousCard.size.width /= 2
      case .J:
            letter = Letter.K
           runAction(SKAction.playSoundFileNamed("11c.mp3", waitForCompletion: false))
            previousCard.texture = SKTexture(imageNamed: "lbc-10.png")
            currentCard.texture = SKTexture(imageNamed: "lbc-11.png")
            upNextCard.texture = SKTexture(imageNamed: "lbc-12.png")
            previousCard.size.width *= 2
      case .K:
            letter = Letter.L
           runAction(SKAction.playSoundFileNamed("12c.mp3", waitForCompletion: false))
            previousCard.texture = SKTexture(imageNamed: "lbc-11.png")
            currentCard.texture = SKTexture(imageNamed: "lbc-12.png")
            upNextCard.texture = SKTexture(imageNamed: "lbc-13.png")
      case .L:
            letter = Letter.M
           runAction(SKAction.playSoundFileNamed("13c.mp3", waitForCompletion: false))
            previousCard.texture = SKTexture(imageNamed: "lbc-12.png")
            currentCard.texture = SKTexture(imageNamed: "lbc-13.png")
            upNextCard.texture = SKTexture(imageNamed: "lbc-14.png")
      case .M:
            letter = Letter.N
            runAction(SKAction.playSoundFileNamed("14c.mp3", waitForCompletion: false))
            previousCard.texture = SKTexture(imageNamed: "lbc-13.png")
            currentCard.texture = SKTexture(imageNamed: "lbc-14.png")
            upNextCard.texture = SKTexture(imageNamed: "lbc-15.png")
      case .N:
            letter = Letter.O
         runAction(SKAction.playSoundFileNamed("15c.mp3", waitForCompletion: false))
            previousCard.texture = SKTexture(imageNamed: "lbc-14.png")
            currentCard.texture = SKTexture(imageNamed: "lbc-15.png")
            upNextCard.texture = SKTexture(imageNamed: "lbc-16.png")
      case .O:
            letter = Letter.P
            runAction(SKAction.playSoundFileNamed("16c.mp3", waitForCompletion: false))
            previousCard.texture = SKTexture(imageNamed: "lbc-15.png")
            currentCard.texture = SKTexture(imageNamed: "lbc-16.png")
            upNextCard.texture = SKTexture(imageNamed: "lbc-17.png")
      case .P:
            letter = Letter.Q
            runAction(SKAction.playSoundFileNamed("17c.mp3", waitForCompletion: false))
            previousCard.texture = SKTexture(imageNamed: "lbc-16.png")
            currentCard.texture = SKTexture(imageNamed: "lbc-17.png")
            upNextCard.texture = SKTexture(imageNamed: "lbc-18.png")
      case .Q:
            letter = Letter.R
            runAction(SKAction.playSoundFileNamed("18c.mp3", waitForCompletion: false))
            previousCard.texture = SKTexture(imageNamed: "lbc-17.png")
            currentCard.texture = SKTexture(imageNamed: "lbc-18.png")
            upNextCard.texture = SKTexture(imageNamed: "lbc-19.png")
      case .R:
        letter = Letter.S
            runAction(SKAction.playSoundFileNamed("19c.mp3", waitForCompletion: false))
            previousCard.texture = SKTexture(imageNamed: "lbc-18.png")
            currentCard.texture = SKTexture(imageNamed: "lbc-19.png")
            upNextCard.texture = SKTexture(imageNamed: "lbc-20.png")
      case .S:
            letter = Letter.T
            runAction(SKAction.playSoundFileNamed("20c.mp3", waitForCompletion: false))
            previousCard.texture = SKTexture(imageNamed: "lbc-19.png")
            currentCard.texture = SKTexture(imageNamed: "lbc-20.png")
            upNextCard.texture = SKTexture(imageNamed: "lbc-21.png")
      case .T:
            letter = Letter.U
           runAction(SKAction.playSoundFileNamed("21c.mp3", waitForCompletion: false))
            previousCard.texture = SKTexture(imageNamed: "lbc-20.png")
            currentCard.texture = SKTexture(imageNamed: "lbc-21.png")
            upNextCard.texture = SKTexture(imageNamed: "lbc-22.png")
      case .U:
            letter = Letter.V
           runAction(SKAction.playSoundFileNamed("22c.mp3", waitForCompletion: false))
            previousCard.texture = SKTexture(imageNamed: "lbc-21.png")
            currentCard.texture = SKTexture(imageNamed: "lbc-22.png")
            upNextCard.texture = SKTexture(imageNamed: "lbc-23.png")
      case .V:
            letter = Letter.W
           runAction(SKAction.playSoundFileNamed("23c.mp3", waitForCompletion: false))
            previousCard.texture = SKTexture(imageNamed: "lbc-22.png")
            currentCard.texture = SKTexture(imageNamed: "lbc-23.png")
            upNextCard.texture = SKTexture(imageNamed: "lbc-24.png")
      case .W:
            letter = Letter.X
            runAction(SKAction.playSoundFileNamed("24c.mp3", waitForCompletion: false))
            previousCard.texture = SKTexture(imageNamed: "lbc-23.png")
            currentCard.texture = SKTexture(imageNamed: "lbc-24.png")
            upNextCard.texture = SKTexture(imageNamed: "lbc-25.png")
      case .X:
            letter = Letter.Y
           runAction(SKAction.playSoundFileNamed("25c.mp3", waitForCompletion: false))
            previousCard.texture = SKTexture(imageNamed: "lbc-24.png")
            currentCard.texture = SKTexture(imageNamed: "lbc-25.png")
            upNextCard.texture = SKTexture(imageNamed: "lbc-26.png")
      case .Y:
            letter = Letter.Z
         runAction(SKAction.playSoundFileNamed("26c.mp3", waitForCompletion: false))
            previousCard.texture = SKTexture(imageNamed: "lbc-25.png")
            currentCard.texture = SKTexture(imageNamed: "lbc-26.png")
            upNextCard.texture = SKTexture(imageNamed: "lbc-1.png")
      case .Z:
            letter = Letter.A
          runAction(SKAction.playSoundFileNamed("1c.mp3", waitForCompletion: false))
            previousCard.texture = SKTexture(imageNamed: "lbc-26.png")
            currentCard.texture = SKTexture(imageNamed: "lbc-1.png")
            upNextCard.texture = SKTexture(imageNamed: "lbc-2.png")
        }
    }
    // function for rendering new image and sound when card is flipped
    func findImageToShow() {
        currentCard.name = "card"
      switch letter {
      case .A:
            if cardFlipped == false {
                cardFlipped = true
                currentCard.texture = SKTexture(imageNamed: "obj-1.png")
                runAction(SKAction.playSoundFileNamed("flash1.mp3", waitForCompletion: false))
            } else {
                cardFlipped = false
                currentCard.texture = SKTexture(imageNamed: "lbc-1.png")
                runAction(SKAction.playSoundFileNamed("1c.mp3", waitForCompletion: false))
            }
       
      case .B:
            if cardFlipped == false {
                cardFlipped = true
                currentCard.texture = SKTexture(imageNamed: "obj-2.png")
                runAction(SKAction.playSoundFileNamed("flash2.mp3", waitForCompletion: false))
            } else {
                cardFlipped = false
                currentCard.texture = SKTexture(imageNamed: "lbc-2.png")
                runAction(SKAction.playSoundFileNamed("2c.mp3", waitForCompletion: false))
            }
        
        
      case .C:
            if cardFlipped == false {
                cardFlipped = true
                currentCard.texture = SKTexture(imageNamed: "obj-3.png")
                runAction(SKAction.playSoundFileNamed("flash3.mp3", waitForCompletion: false))
            } else {
                cardFlipped = false
                currentCard.texture = SKTexture(imageNamed: "lbc-3.png")

                runAction(SKAction.playSoundFileNamed("3c.mp3", waitForCompletion: false))
            }
      case .D:
      
            if cardFlipped == false {
                cardFlipped = true
                currentCard.texture = SKTexture(imageNamed: "obj-4.png")
                runAction(SKAction.playSoundFileNamed("flash4.mp3", waitForCompletion: false))
            } else {
                cardFlipped = false
                currentCard.texture = SKTexture(imageNamed: "lbc-4.png")
                runAction(SKAction.playSoundFileNamed("4c.mp3", waitForCompletion: false))
            }
      case .E:
            if cardFlipped == false {
                cardFlipped = true
                currentCard.texture = SKTexture(imageNamed: "obj-5.png")
                runAction(SKAction.playSoundFileNamed("flash5.mp3", waitForCompletion: false))
            } else {
                cardFlipped = false
                currentCard.texture = SKTexture(imageNamed: "lbc-5.png")
                runAction(SKAction.playSoundFileNamed("5c.mp3", waitForCompletion: false))
            }
      case .F:
            if cardFlipped == false {
                cardFlipped = true
                currentCard.texture = SKTexture(imageNamed: "obj-6.png")
                runAction(SKAction.playSoundFileNamed("flash6.mp3", waitForCompletion: false))
            } else {
                cardFlipped = false
                currentCard.texture = SKTexture(imageNamed: "lbc-6.png")
                runAction(SKAction.playSoundFileNamed("6c.mp3", waitForCompletion: false))
            }
      case .G:
            if cardFlipped == false {
                cardFlipped = true
                currentCard.texture = SKTexture(imageNamed: "obj-7.png")
                runAction(SKAction.playSoundFileNamed("flash7.mp3", waitForCompletion: false))
            } else {
                cardFlipped = false
                currentCard.texture = SKTexture(imageNamed: "lbc-7.png")
                runAction(SKAction.playSoundFileNamed("7c.mp3", waitForCompletion: false))
            }
      case .H:
            if cardFlipped == false {
                cardFlipped = true
                currentCard.texture = SKTexture(imageNamed: "obj-8.png")
                runAction(SKAction.playSoundFileNamed("flash8.mp3", waitForCompletion: false))
            } else {
                cardFlipped = false
                currentCard.texture = SKTexture(imageNamed: "lbc-8.png")
                runAction(SKAction.playSoundFileNamed("8c.mp3", waitForCompletion: false))
            }
      case .I:
            if cardFlipped == false {
                cardFlipped = true
                currentCard.texture = SKTexture(imageNamed: "obj-9.png")
                runAction(SKAction.playSoundFileNamed("flash9.mp3", waitForCompletion: false))
            } else {
                cardFlipped = false
                currentCard.texture = SKTexture(imageNamed: "lbc-9.png")
                runAction(SKAction.playSoundFileNamed("9c.mp3", waitForCompletion: false))
            }
      case .J:
            if cardFlipped == false {
                cardFlipped = true
                currentCard.texture = SKTexture(imageNamed: "obj-10.png")
                runAction(SKAction.playSoundFileNamed("flash10.mp3", waitForCompletion: false))
            } else {
                cardFlipped = false
                currentCard.texture = SKTexture(imageNamed: "lbc-10.png")
                runAction(SKAction.playSoundFileNamed("10c.mp3", waitForCompletion: false))
            }
      case .K:
            if cardFlipped == false {
                cardFlipped = true
                currentCard.texture = SKTexture(imageNamed: "obj-11.png")
                runAction(SKAction.playSoundFileNamed("flash11.mp3", waitForCompletion: false))
            } else {
                cardFlipped = false
                currentCard.texture = SKTexture(imageNamed: "lbc-11.png")
                runAction(SKAction.playSoundFileNamed("11c.mp3", waitForCompletion: false))
            }
      case .L:
            if cardFlipped == false {
                cardFlipped = true
                currentCard.texture = SKTexture(imageNamed: "obj-12.png")
                runAction(SKAction.playSoundFileNamed("flash12.mp3", waitForCompletion: false))
            } else {
                cardFlipped = false
                currentCard.texture = SKTexture(imageNamed: "lbc-12.png")
                runAction(SKAction.playSoundFileNamed("12c.mp3", waitForCompletion: false))
            }
      case .M:
            if cardFlipped == false {
                cardFlipped = true
                currentCard.texture = SKTexture(imageNamed: "obj-13.png")
                runAction(SKAction.playSoundFileNamed("flash13.mp3", waitForCompletion: false))
            } else {
                cardFlipped = false
                currentCard.texture = SKTexture(imageNamed: "lbc-13.png")
                runAction(SKAction.playSoundFileNamed("13c.mp3", waitForCompletion: false))
            }
      case .N:
            if cardFlipped == false {
                cardFlipped = true
                currentCard.texture = SKTexture(imageNamed: "obj-14.png")
                runAction(SKAction.playSoundFileNamed("flash14.mp3", waitForCompletion: false))
            } else {
                cardFlipped = false
                currentCard.texture = SKTexture(imageNamed: "lbc-14.png")
                runAction(SKAction.playSoundFileNamed("14c.mp3", waitForCompletion: false))
            }
      case .O:
            if cardFlipped == false {
                cardFlipped = true
                currentCard.texture = SKTexture(imageNamed: "obj-15.png")
                runAction(SKAction.playSoundFileNamed("flash15.mp3", waitForCompletion: false))
            } else {
                cardFlipped = false
                currentCard.texture = SKTexture(imageNamed: "lbc-15.png")
                runAction(SKAction.playSoundFileNamed("15c.mp3", waitForCompletion: false))
            }
      case .P:
            if cardFlipped == false {
                cardFlipped = true
                currentCard.texture = SKTexture(imageNamed: "obj-16.png")
                runAction(SKAction.playSoundFileNamed("flash16.mp3", waitForCompletion: false))
            } else {
                cardFlipped = false
                currentCard.texture = SKTexture(imageNamed: "lbc-16.png")
                runAction(SKAction.playSoundFileNamed("16c.mp3", waitForCompletion: false))
            }
      case .Q:
            if cardFlipped == false {
                cardFlipped = true
                currentCard.texture = SKTexture(imageNamed: "obj-17.png")
                runAction(SKAction.playSoundFileNamed("flash17.mp3", waitForCompletion: false))
            } else {
                cardFlipped = false
                currentCard.texture = SKTexture(imageNamed: "lbc-17.png")
                runAction(SKAction.playSoundFileNamed("17c.mp3", waitForCompletion: false))
            }
        case .R:
            if cardFlipped == false {
                cardFlipped = true
                currentCard.texture = SKTexture(imageNamed: "obj-18.png")
                runAction(SKAction.playSoundFileNamed("flash18.mp3", waitForCompletion: false))
            } else {
                cardFlipped = false
                currentCard.texture = SKTexture(imageNamed: "lbc-18.png")
                runAction(SKAction.playSoundFileNamed("18c.mp3", waitForCompletion: false))
            }
      case .S:
            if cardFlipped == false {
                cardFlipped = true
                currentCard.texture = SKTexture(imageNamed: "obj-19.png")
                runAction(SKAction.playSoundFileNamed("flash19.mp3", waitForCompletion: false))
            } else {
                cardFlipped = false
                currentCard.texture = SKTexture(imageNamed: "lbc-19.png")
                runAction(SKAction.playSoundFileNamed("19c.mp3", waitForCompletion: false))
            }
      case .T:
            if cardFlipped == false {
                cardFlipped = true
                currentCard.texture = SKTexture(imageNamed: "obj-20.png")
                runAction(SKAction.playSoundFileNamed("flash20.mp3", waitForCompletion: false))
            } else {
                cardFlipped = false
                currentCard.texture = SKTexture(imageNamed: "lbc-20.png")
                runAction(SKAction.playSoundFileNamed("20c.mp3", waitForCompletion: false))
            }
      case .U:
            if cardFlipped == false {
                cardFlipped = true
                currentCard.texture = SKTexture(imageNamed: "obj-21.png")
                runAction(SKAction.playSoundFileNamed("flash21.mp3", waitForCompletion: false))
            } else {
                cardFlipped = false
                currentCard.texture = SKTexture(imageNamed: "lbc-21.png")
                runAction(SKAction.playSoundFileNamed("21c.mp3", waitForCompletion: false))
            }
      case .V:
            if cardFlipped == false {
                cardFlipped = true
                currentCard.texture = SKTexture(imageNamed: "obj-22.png")
                runAction(SKAction.playSoundFileNamed("flash22.mp3", waitForCompletion: false))
            } else {
                cardFlipped = false
                currentCard.texture = SKTexture(imageNamed: "lbc-22.png")
                runAction(SKAction.playSoundFileNamed("22c.mp3", waitForCompletion: false))
            }
      case .W:
            if cardFlipped == false {
                cardFlipped = true
                currentCard.texture = SKTexture(imageNamed: "obj-23.png")
                runAction(SKAction.playSoundFileNamed("flash23.mp3", waitForCompletion: false))
            } else {
                cardFlipped = false
                currentCard.texture = SKTexture(imageNamed: "lbc-23.png")
                runAction(SKAction.playSoundFileNamed("23c.mp3", waitForCompletion: false))
            }
      case .X:
            if cardFlipped == false {
                cardFlipped = true
                currentCard.texture = SKTexture(imageNamed: "obj-24.png")
                runAction(SKAction.playSoundFileNamed("flash24.mp3", waitForCompletion: false))
            } else {
                cardFlipped = false
                currentCard.texture = SKTexture(imageNamed: "lbc-24.png")
                runAction(SKAction.playSoundFileNamed("24c.mp3", waitForCompletion: false))
            }
      case .Y:
            if cardFlipped == false {
                cardFlipped = true
                currentCard.texture = SKTexture(imageNamed: "obj-25.png")
                runAction(SKAction.playSoundFileNamed("flash25.mp3", waitForCompletion: false))
            } else {
                cardFlipped = false
                currentCard.texture = SKTexture(imageNamed: "lbc-25.png")
                runAction(SKAction.playSoundFileNamed("25c.mp3", waitForCompletion: false))
            }
      case .Z:
            if cardFlipped == false {
                cardFlipped = true
                currentCard.texture = SKTexture(imageNamed: "obj-26.png")
                runAction(SKAction.playSoundFileNamed("flash26.mp3", waitForCompletion: false))
            } else  {
                cardFlipped = false
                currentCard.texture = SKTexture(imageNamed: "lbc-26.png")
                runAction(SKAction.playSoundFileNamed("26c.mp3", waitForCompletion: false))
            }
         
      }
    }
    
    override func didMoveToView(view: SKView) {
        
        bgImage.position = CGPointMake(self.size.width / 2, self.size.height / 2)
        bgImage.zPosition = 1
        self.addChild(bgImage)
        //back button
        back.position = CGPoint(x: 40,y: 720)
        back.name = "back"
        back.userInteractionEnabled = false
        back.zPosition = 2
        self.addChild(back)
        //for card
        currentCard.position = CGPoint(x: CGRectGetMidX(self.frame), y: CGRectGetMidY(self.frame))
        currentCard.zPosition = 3
        currentCard.name = "card"
        self.addChild(currentCard)
        //for card background
        currentCardBackground.position = CGPoint(x: CGRectGetMidX(self.frame), y: CGRectGetMidY(self.frame))
        currentCardBackground.zPosition = 2
        currentCardBackground.name = "cardBackground"
        self.addChild(currentCardBackground)
        
        upNextCard.position = CGPoint(x: self.frame.size.width * 0.85, y: CGRectGetMidY(self.frame))
        upNextCard.zPosition = 3
      upNextCard.name = "upNextCard"
        self.addChild(upNextCard)
        
        previousCard.position = CGPoint(x: self.frame.size.width * 0.15, y: CGRectGetMidY(self.frame))
        previousCard.zPosition = 3
      previousCard.name = "previousCard"
        self.addChild(previousCard)
        
        let shrink = SKAction.scaleBy(0.5, duration: 0)
        upNextCard.runAction(shrink)
        previousCard.runAction(shrink)
        //forward arrow
        forwardArrow.position = CGPoint(x: self.frame.size.width * 0.6, y: self.frame.size.height * 0.1)
        forwardArrow.zPosition = 2
        forwardArrow.name = "Forward"
        self.addChild(forwardArrow)
        //backwad arrow
        backwardArrow.position = CGPoint(x: self.frame.size.width * 0.4, y: self.frame.size.height * 0.1)
        backwardArrow.zPosition = 2
        backwardArrow.name = "Backward"
        self.addChild(backwardArrow)
        
        
        previousCard.hidden = true
        backwardArrow.hidden = true

      
    }
    //actions performed when nodes are touched on screen
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        for touch in touches {
            let location = touch.locationInNode(self)
            let touchedNode = self.nodeAtPoint(location)
            
            if let name = touchedNode.name {
                if name == "back" {
                
                    let transition = SKTransition.crossFadeWithDuration(1.0)
                    let backScene = GameScene(size: self.size)
                  //backScene.backgroundMusic.addChild(<#T##node: SKNode##SKNode#>)
                    self.view!.presentScene(backScene, transition: transition)
                  
                     
                }
                
                if name  == "card" {
                  var timer = NSTimer.scheduledTimerWithTimeInterval(0.75, target: self, selector: "findImageToShow", userInfo: nil, repeats: false)
                  currentCard.name = ""
                }
                
               if name == "Forward" ||  name == "upNextCard"{
               var timer = NSTimer.scheduledTimerWithTimeInterval(1, target: self, selector: "addLetter", userInfo: nil, repeats: false)
                    forwardArrow.name = ""
                  backwardArrow.name = ""
                  previousCard.name = ""
                  upNextCard.name = ""
                  
                }
               
          
                if name == "Backward" || name == "previousCard"{
                  var timer = NSTimer.scheduledTimerWithTimeInterval(1, target: self, selector: "subtractLetter", userInfo: nil, repeats: false)
                 
                  forwardArrow.name = ""
                  backwardArrow.name = ""
                  previousCard.name = ""
                  upNextCard.name = ""
                }
            }
        
      }
    
}
}

