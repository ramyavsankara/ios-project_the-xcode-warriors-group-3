//
//  Numbers.swift
//  Fun to Learn!!
//
//  Created by Gade,Srujan Mathur on 4/14/16.
//  Copyright © 2016 Sankara,Ramya Virajitha. All rights reserved.
//

import SpriteKit
//Renders logic for Numbers Scene
class Numbers: SKScene {
    
    let bgImage = SKSpriteNode(imageNamed: "bg-intro.png")
    let back = SKSpriteNode(imageNamed: "bt_Back.png")
    
    let currentCardBackground = SKSpriteNode(imageNamed: "card1.png")
    let currentCard = SKSpriteNode(imageNamed: "card01_zero.png")
    let upNextCardBackground = SKSpriteNode(imageNamed: "")
    let upNextCard = SKSpriteNode(imageNamed: "card01_one.png")
    let previousCard = SKSpriteNode(imageNamed: "card01_nine.png")
    
    let forwardArrow = SKSpriteNode(imageNamed: "bt_Arrow_Forward.png")
    let backwardArrow = SKSpriteNode(imageNamed: "bt_Arrow_Backward.png")
    
    var number = 0
    
    var cardFlipped = false
    //function for adding image from scene
    func addNumber() {
        cardFlipped = false
        forwardArrow.name = "Forward"
        backwardArrow.name = "Backward"
        upNextCard.name = "upNextCard"
        previousCard.name = "previousCard"
        
        switch number {
        case 0:
            number = 1
            previousCard.texture = SKTexture(imageNamed: "card01_zero.png")
            currentCard.texture = SKTexture(imageNamed: "card01_one.png")
            upNextCard.texture = SKTexture(imageNamed: "card01_two.png")
            previousCard.hidden = false
            backwardArrow.hidden = false
            
        
        case 1:
            number = 2
            previousCard.texture = SKTexture(imageNamed: "card01_one.png")
            currentCard.texture = SKTexture(imageNamed: "card01_two.png")
            upNextCard.texture = SKTexture(imageNamed: "card01_three.png")
           
        case 2:
            number = 3
            previousCard.texture = SKTexture(imageNamed: "card01_two.png")
            currentCard.texture = SKTexture(imageNamed: "card01_three.png")
            upNextCard.texture = SKTexture(imageNamed: "card01_four.png")
            
        case 3:
            number = 4
            previousCard.texture = SKTexture(imageNamed: "card01_three.png")
            currentCard.texture = SKTexture(imageNamed: "card01_four.png")
            upNextCard.texture = SKTexture(imageNamed: "card01_five.png")
            
        case 4:
            number = 5
            previousCard.texture = SKTexture(imageNamed: "card01_four.png")
            currentCard.texture = SKTexture(imageNamed: "card01_five.png")
            upNextCard.texture = SKTexture(imageNamed: "card01_six.png")
        case 5:
            number = 6
            previousCard.texture = SKTexture(imageNamed: "card01_five.png")
            currentCard.texture = SKTexture(imageNamed: "card01_six.png")
            upNextCard.texture = SKTexture(imageNamed: "card01_seven.png")
        case 6:
            number = 7
            previousCard.texture = SKTexture(imageNamed: "card01_six.png")
            currentCard.texture = SKTexture(imageNamed: "card01_seven.png")
            upNextCard.texture = SKTexture(imageNamed: "card01_eight.png")
        case 7:
            number = 8
            previousCard.texture = SKTexture(imageNamed: "card01_seven.png")
            currentCard.texture = SKTexture(imageNamed: "card01_eight.png")
            upNextCard.texture = SKTexture(imageNamed: "card01_nine.png")
        case 8:
            number = 9
            previousCard.texture = SKTexture(imageNamed: "card01_eight.png")
            currentCard.texture = SKTexture(imageNamed: "card01_nine.png")
            upNextCard.texture = SKTexture(imageNamed: "card01_zero.png")
        case 9:
            number = 0
            previousCard.texture = SKTexture(imageNamed: "card01_nine.png")
            currentCard.texture = SKTexture(imageNamed: "card01_zero.png")
            upNextCard.texture = SKTexture(imageNamed: "card01_one.png")
            
        default: break
            
        }
    }
    //function for removing image from scene
    func subtractNumber() {
        cardFlipped = false
        forwardArrow.name = "Forward"
        backwardArrow.name = "Backward"
        upNextCard.name = "upNextCard"
        previousCard.name = "previousCard"
        
        switch number {
        case 1:
            number = 0
            upNextCard.texture = SKTexture(imageNamed: "card01_one.png")
            currentCard.texture = SKTexture(imageNamed: "card01_zero.png")
            previousCard.texture = SKTexture(imageNamed: "card01_nine.png")
            previousCard.hidden = true
            backwardArrow.hidden = true
            
        case 2:
            number = 1
            upNextCard.texture = SKTexture(imageNamed: "card01_two.png")
            currentCard.texture = SKTexture(imageNamed: "card01_one.png")
            previousCard.texture = SKTexture(imageNamed: "card01_zero.png")
            
        case 3:
            number = 2
            upNextCard.texture = SKTexture(imageNamed: "card01_three.png")
            currentCard.texture = SKTexture(imageNamed: "card01_two.png")
            previousCard.texture = SKTexture(imageNamed: "card01_one.png")
            
        case 4:
            number = 3
            upNextCard.texture = SKTexture(imageNamed: "card01_four.png")
            currentCard.texture = SKTexture(imageNamed: "card01_three.png")
            previousCard.texture = SKTexture(imageNamed: "card01_two.png")
            
        case 5:
            number = 4
            upNextCard.texture = SKTexture(imageNamed: "card01_five.png")
            currentCard.texture = SKTexture(imageNamed: "card01_four.png")
            previousCard.texture = SKTexture(imageNamed: "card01_three.png")
            
        case 6:
            number = 5
            upNextCard.texture = SKTexture(imageNamed: "card01_six.png")
            currentCard.texture = SKTexture(imageNamed: "card01_five.png")
            previousCard.texture = SKTexture(imageNamed: "card01_four.png")
            
        case 7:
            number = 6
            upNextCard.texture = SKTexture(imageNamed: "card01_seven.png")
            currentCard.texture = SKTexture(imageNamed: "card01_six.png")
            previousCard.texture = SKTexture(imageNamed: "card01_five.png")
        
        case 8:
            number = 7
            upNextCard.texture = SKTexture(imageNamed: "card01_eight.png")
            currentCard.texture = SKTexture(imageNamed: "card01_seven.png")
            previousCard.texture = SKTexture(imageNamed: "card01_six.png")
            
        case 9:
            number = 8
            upNextCard.texture = SKTexture(imageNamed: "card01_nine.png")
            currentCard.texture = SKTexture(imageNamed: "card01_eight.png")
            previousCard.texture = SKTexture(imageNamed: "card01_seven.png")
            
        default:break
        }
    }
    // function for rendering new image and sound when card is flipped
    func flipNumberCard() {
        runAction(SKAction.playSoundFileNamed("cardflip.mp3", waitForCompletion: false))
         currentCard.name = "card"
        switch number {
        case 0:
            if cardFlipped == false {
                cardFlipped = true
                currentCard.texture = SKTexture(imageNamed: "card01_zero.png")
                runAction(SKAction.playSoundFileNamed("sound_0", waitForCompletion: false))
            } else {
                cardFlipped = false
                currentCard.texture = SKTexture(imageNamed: "card01_zeroback.png")
                runAction(SKAction.playSoundFileNamed("sound_0", waitForCompletion: false))
            }
        case 1:
            if cardFlipped == false {
                cardFlipped = true
                currentCard.texture = SKTexture(imageNamed: "card01_one.png")
                runAction(SKAction.playSoundFileNamed("sound_1", waitForCompletion: false))
            } else {
                cardFlipped = false
                currentCard.texture = SKTexture(imageNamed: "card01_oneback.png")
                runAction(SKAction.playSoundFileNamed("sound_1", waitForCompletion: false))

            }
        case 2:
            if cardFlipped == false {
                cardFlipped = true
                currentCard.texture = SKTexture(imageNamed: "card01_two.png")
                runAction(SKAction.playSoundFileNamed("sound_2", waitForCompletion: false))
            } else {
                cardFlipped = false
                currentCard.texture = SKTexture(imageNamed: "card01_twoback.png")
                runAction(SKAction.playSoundFileNamed("sound_2", waitForCompletion: false))
            }
        case 3:
            if cardFlipped == false {
                cardFlipped = true
                currentCard.texture = SKTexture(imageNamed: "card01_three.png")
                runAction(SKAction.playSoundFileNamed("sound_3", waitForCompletion: false))
                
            } else {
               cardFlipped = false
               
                currentCard.texture = SKTexture(imageNamed: "card01_threeback.png")
                runAction(SKAction.playSoundFileNamed("sound_3", waitForCompletion: false))

            }
        case 4:
            if cardFlipped == false {
                cardFlipped = true
                currentCard.texture = SKTexture(imageNamed: "card01_four.png")
                runAction(SKAction.playSoundFileNamed("sound_4", waitForCompletion: false))
 
                
            } else {
                cardFlipped = false
                cardFlipped = false
                currentCard.texture = SKTexture(imageNamed: "card01_fourback.png")
                runAction(SKAction.playSoundFileNamed("sound_4", waitForCompletion: false))
                
    
            }
        case 5:
            if cardFlipped == false {
                cardFlipped = true
                currentCard.texture = SKTexture(imageNamed: "card01_five.png")
                runAction(SKAction.playSoundFileNamed("sound_5", waitForCompletion: false))
                
                
            } else {
                cardFlipped = false
                currentCard.texture = SKTexture(imageNamed: "card01_fiveback.png")
                runAction(SKAction.playSoundFileNamed("sound_5", waitForCompletion: false))
                
            }
        case 6:
            if cardFlipped == false {
                cardFlipped = true
                currentCard.texture = SKTexture(imageNamed: "card01_six.png")
                runAction(SKAction.playSoundFileNamed("sound_6", waitForCompletion: false))
                
                
            } else {
                cardFlipped = false
                currentCard.texture = SKTexture(imageNamed: "card01_sixback.png")
                runAction(SKAction.playSoundFileNamed("sound_6", waitForCompletion: false))
                
            }
        case 7:
            if cardFlipped == false {
                cardFlipped = true
                currentCard.texture = SKTexture(imageNamed: "card01_seven.png")
                runAction(SKAction.playSoundFileNamed("sound_7", waitForCompletion: false))
                
            } else {
                cardFlipped = false
                currentCard.texture = SKTexture(imageNamed: "card01_sevenback.png")
                runAction(SKAction.playSoundFileNamed("sound_7", waitForCompletion: false))
            }
        case 8:
            if cardFlipped == false {
                cardFlipped = true
                currentCard.texture = SKTexture(imageNamed: "card01_eight.png")
                runAction(SKAction.playSoundFileNamed("sound_8", waitForCompletion: false))
            } else {
                cardFlipped = false
                currentCard.texture = SKTexture(imageNamed: "card01_eightback.png")
                runAction(SKAction.playSoundFileNamed("sound_8", waitForCompletion: false))
            }
        case 9:
            if cardFlipped == false {
                cardFlipped = true
                currentCard.texture = SKTexture(imageNamed: "card01_nine.png")
                runAction(SKAction.playSoundFileNamed("sound_9", waitForCompletion: false))
            } else {
                cardFlipped = false
                currentCard.texture = SKTexture(imageNamed: "card01_nineback.png")
                runAction(SKAction.playSoundFileNamed("sound_9", waitForCompletion: false))
            }
        default: break
        }
        
    }
    
    override func didMoveToView(view: SKView) {
        
        bgImage.position = CGPointMake(self.size.width / 2, self.size.height / 2)
        bgImage.zPosition = 1
        self.addChild(bgImage)
        
        back.position = CGPoint(x: 40,y: 720)
        back.name = "back"
        back.userInteractionEnabled = false
        back.zPosition = 2
        self.addChild(back)
        
        previousCard.position = CGPoint(x: self.frame.size.width * 0.15, y: CGRectGetMidY(self.frame))
         previousCard.zPosition = 3
        previousCard.name = "previousCard"
        self.addChild(previousCard)
        
        currentCard.position = CGPoint(x: CGRectGetMidX(self.frame), y: CGRectGetMidY(self.frame))
        currentCard.name = "card"
        currentCard.zPosition = 3
        self.addChild(currentCard)
        
        currentCardBackground.position = CGPoint(x: CGRectGetMidX(self.frame), y: CGRectGetMidY(self.frame))
        currentCardBackground.zPosition = 2
        currentCardBackground.name = "cardBackground"
        self.addChild(currentCardBackground)
        
        upNextCard.position = CGPoint(x: self.frame.size.width * 0.85, y: CGRectGetMidY(self.frame))
       upNextCard.name = "upNextCard"
        upNextCard.zPosition = 3
        self.addChild(upNextCard)
        let shrink = SKAction.scaleBy(0.6, duration: 0)
        upNextCard.runAction(SKAction.scaleBy(0.3, duration: 0))
        
        previousCard.runAction(SKAction.scaleBy(0.3, duration: 0))
        currentCard.runAction(shrink)
        forwardArrow.position = CGPoint(x: self.frame.size.width * 0.6, y: self.frame.size.height * 0.1)
        forwardArrow.zPosition = 2
        forwardArrow.name = "Forward"
        self.addChild(forwardArrow)
        
        backwardArrow.position = CGPoint(x: self.frame.size.width * 0.4, y: self.frame.size.height * 0.1)
        backwardArrow.zPosition = 2
        backwardArrow.name = "Backward"
        self.addChild(backwardArrow)
        
        
        previousCard.hidden = true
        backwardArrow.hidden = true
    }
    //actions performed when nodes are touched on screen
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        for touch in touches {
            let location = touch.locationInNode(self)
            let touchedNode = self.nodeAtPoint(location)
            
            if let name = touchedNode.name {
                if name == "back" {
                    print("in back touch")
                    let transition = SKTransition.crossFadeWithDuration(1.0)
                    let backScene = GameScene(size: self.size)
                    self.view!.presentScene(backScene, transition: transition)
                }
                
                if name == "card" || name == "cardBackground" {
                    flipNumberCard()
                }
                
                if name == "Forward" || name == "upNextCard"{
                    addNumber()
                }
                
                if name == "Backward" || name == "previousCard"{
                    subtractNumber()
                }
            }
        }
        
    }
    
}
