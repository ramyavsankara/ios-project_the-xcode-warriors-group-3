//
//  PlayScene.swift
//  Fun to Learn!!
//
//  Created by Gade,Srujan Mathur on 3/18/16.
//  Copyright © 2016 Sankara,Ramya Virajitha. All rights reserved.
//

import SpriteKit
// Play scene where games are rendered
class PlayScene: SKScene, UITextFieldDelegate {
    
    let bgImage = SKSpriteNode(imageNamed: "bg-intro.png")
    let back = SKSpriteNode(imageNamed: "bt_Back.png")
    
    var countingGame = false
    let countingTextField = UITextField()

    let objectToCount = SKSpriteNode(imageNamed: "image_1.png")
    
    var correctCounting = "1"
    //randomly gets the image for game(counting)
    func getObjectToCount() {
        let random = Int(arc4random_uniform(9))
        
        if random == 0 {
            objectToCount.texture = SKTexture(imageNamed: "image_1.png")
            correctCounting = "1"
        } else if random == 1 {
            objectToCount.texture = SKTexture(imageNamed: "image_2.png")
            correctCounting = "2"
        } else if random == 2 {
            objectToCount.texture = SKTexture(imageNamed: "image_3.png")
            correctCounting = "3"
        } else if random == 3 {
            objectToCount.texture = SKTexture(imageNamed: "image_4.png")
            correctCounting = "4"
        } else if random == 4 {
            objectToCount.texture = SKTexture(imageNamed: "image_5.png")
            correctCounting = "5"
        } else if random == 5 {
            objectToCount.texture = SKTexture(imageNamed: "image_6.png")
            correctCounting = "6"
        } else if random == 6 {
            objectToCount.texture = SKTexture(imageNamed: "image_7.png")
            correctCounting = "7"
        } else if random == 7 {
            objectToCount.texture = SKTexture(imageNamed: "image_8.png")
            correctCounting = "8"
        } else if random == 8 {
            objectToCount.texture = SKTexture(imageNamed: "image_9.png")
            correctCounting = "9"
        }
    }
    
    func setupCountingGame() {
        countingGame = true
        
        objectToCount.position = CGPoint(x:CGRectGetMidX(self.frame),y: 550)
        objectToCount.zPosition = 2
        self.addChild(objectToCount)
        let shrink = SKAction.scaleBy(0.75, duration: 0)
        objectToCount.runAction(shrink)
        //countingTextField.center = CGPointMake(CGRectGetMidX(self.frame), 800)
        countingTextField.frame = CGRect(x: CGRectGetMidX(self.frame), y: 200,
            width: 200, height: 40)
        //var point:CGPoint = CGPointMake(CGRectGetMidX(self.frame), 800)

        countingTextField.center = self.view!.center
        countingTextField.borderStyle = UITextBorderStyle.RoundedRect
        countingTextField.textColor = SKColor.blackColor()
        countingTextField.font = UIFont.systemFontOfSize(20)
        countingTextField.placeholder = "Enter Number"
        countingTextField.backgroundColor = SKColor.whiteColor()
        countingTextField.autocorrectionType = UITextAutocorrectionType.No
        countingTextField.keyboardType = UIKeyboardType.NumberPad
        countingTextField.delegate = self
        self.view?.addSubview(countingTextField)
        
        getObjectToCount()
    }


    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        /* Called when a touch begins */
        
        for touch in touches {
            let location = touch.locationInNode(self)
            let touchedNode = self.nodeAtPoint(location)
            
            if let name = touchedNode.name
            {
                if name == "back"
                {
                    print("in back touch")
                    let transition = SKTransition.crossFadeWithDuration(1.0)
                    let backScene = GameScene(size: self.size)
                    self.view!.presentScene(backScene, transition: transition)
                    countingTextField.resignFirstResponder()
                    countingTextField.removeFromSuperview()
                }
            }
        }
    }
    override func didMoveToView(view: SKView) {
        /* Setup your scene here */
         setupCountingGame()
        bgImage.position = CGPointMake(self.size.width/2, self.size.height/2)
        bgImage.zPosition=1
        back.position = CGPoint(x:40,y: 720)
        back.name = "back"
        back.userInteractionEnabled = false
        back.zPosition = 2
        self.addChild(bgImage)
        self.addChild(back)
        
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
     // for textfield input
        
        
        if countingGame == true {
            if textField.text == correctCounting {
                textField.resignFirstResponder()
                textField.removeFromSuperview()
                // clap musics
                print("success")
                let transition = SKTransition.crossFadeWithDuration(1.0)
                let playScene = PlayScene(size: self.size)
                self.view!.presentScene(playScene, transition: transition)
            } else {
                //no music
                print("sorry")
            }
        }
        
        return true
    }

    override func update(currentTime: CFTimeInterval) {
        /* Called before each frame is rendered */
    }


}
