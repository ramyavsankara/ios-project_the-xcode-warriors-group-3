//
//  Colors.swift
//  Fun to Learn!!
//
//  Created by Sama,Sainath Reddy on 4/14/16.
//  Copyright © 2016 Sankara,Ramya Virajitha. All rights reserved.
//

import SpriteKit
//Renders logic for colors scene
class Colors: SKScene {
    
    let bgImage = SKSpriteNode(imageNamed: "bg-intro.png")
    let back = SKSpriteNode(imageNamed: "bt_Back.png")
    enum Color {
        case Orange
        case Blue
        case Red
        case Yellow
        case Black
        case Green
        case Purple
        case Pink
        case Brown
        case White
        
    }
     var color = Color.Blue
    
    let currentCardBackground = SKSpriteNode(imageNamed: "card1.png")
    let currentCard = SKSpriteNode(imageNamed: "blue.png")
    let upNextCardBackground = SKSpriteNode(imageNamed: "")
    let upNextCard = SKSpriteNode(imageNamed: "orange.png")
    let previousCard = SKSpriteNode(imageNamed: "white.png")

    let forwardArrow = SKSpriteNode(imageNamed: "bt_Arrow_Forward.png")
    let backwardArrow = SKSpriteNode(imageNamed: "bt_Arrow_Backward.png")
    var cardFlipped = false
    //function for removing image from scene
    func subtractColor() {
        cardFlipped = false
        forwardArrow.name = "Forward"
        backwardArrow.name = "Backward"
        upNextCard.name = "upNextCard"
        previousCard.name = "previousCard"

        switch color {
        case .Blue:
            color =  Color.Orange
            
            upNextCard.texture = SKTexture(imageNamed: "blue.png")
            currentCard.texture = SKTexture(imageNamed: "orange.png")
            previousCard.texture = SKTexture(imageNamed: "white.png")
            previousCard.hidden = true
            backwardArrow.hidden = true
            
        case .Red:
            color = Color.Blue
            //runAction(SKAction.playSoundFileNamed("2c.mp3", waitForCompletion: false))
            upNextCard.texture = SKTexture(imageNamed: "red.png")
            currentCard.texture = SKTexture(imageNamed: "blue.png")
            previousCard.texture = SKTexture(imageNamed: "orange.png")
        case .Yellow:
            color = Color.Red
            //runAction(SKAction.playSoundFileNamed("3c.mp3", waitForCompletion: false))
            upNextCard.texture = SKTexture(imageNamed: "yellow.png")
            currentCard.texture = SKTexture(imageNamed: "red.png")
            previousCard.texture = SKTexture(imageNamed: "blue.png")
        case .Black:
                color = Color.Yellow
                //runAction(SKAction.playSoundFileNamed("3c.mp3", waitForCompletion: false))
                upNextCard.texture = SKTexture(imageNamed: "black.png")
                currentCard.texture = SKTexture(imageNamed: "yellow.png")
                previousCard.texture = SKTexture(imageNamed: "red.png")
        case .Green:
                color = Color.Black
                //runAction(SKAction.playSoundFileNamed("3c.mp3", waitForCompletion: false))
                upNextCard.texture = SKTexture(imageNamed: "green.png")
                currentCard.texture = SKTexture(imageNamed: "black.png")
                previousCard.texture = SKTexture(imageNamed: "yellow.png")
        case .Purple:
            color = Color.Green
            //runAction(SKAction.playSoundFileNamed("3c.mp3", waitForCompletion: false))
            upNextCard.texture = SKTexture(imageNamed: "purple.png")
            currentCard.texture = SKTexture(imageNamed: "green.png")
            previousCard.texture = SKTexture(imageNamed: "black.png")
        case .Pink:
            color = Color.Purple
            //runAction(SKAction.playSoundFileNamed("3c.mp3", waitForCompletion: false))
            upNextCard.texture = SKTexture(imageNamed: "pink.png")
            currentCard.texture = SKTexture(imageNamed: "purple.png")
            previousCard.texture = SKTexture(imageNamed: "green.png")
        case .Brown:
            color = Color.Pink
            //runAction(SKAction.playSoundFileNamed("3c.mp3", waitForCompletion: false))
            upNextCard.texture = SKTexture(imageNamed: "brown.png")
            currentCard.texture = SKTexture(imageNamed: "pink.png")
            previousCard.texture = SKTexture(imageNamed: "purple.png")
        case .White:
            color = Color.Brown
            //runAction(SKAction.playSoundFileNamed("3c.mp3", waitForCompletion: false))
            upNextCard.texture = SKTexture(imageNamed: "white.png")
            currentCard.texture = SKTexture(imageNamed: "brown.png")
            previousCard.texture = SKTexture(imageNamed: "pink.png")
            
        default: break
        }
    }
       //function for adding image from scene
        func addColor() {
            cardFlipped = false
            forwardArrow.name = "Forward"
            backwardArrow.name = "Backward"
            upNextCard.name = "upNextCard"
            previousCard.name = "previousCard"
            
            switch color {
            case .Orange:
                color = Color.Blue
                //runAction(SKAction.playSoundFileNamed("2c.mp3", waitForCompletion: false))
                previousCard.texture = SKTexture(imageNamed: "orange.png")
                currentCard.texture = SKTexture(imageNamed: "blue.png")
                upNextCard.texture = SKTexture(imageNamed: "red.png")
                previousCard.hidden = false
                backwardArrow.hidden = false
            case .Blue:
                color = Color.Red
                //runAction(SKAction.playSoundFileNamed("3c.mp3", waitForCompletion: false))
                previousCard.texture = SKTexture(imageNamed: "blue.png")
                currentCard.texture = SKTexture(imageNamed: "red.png")
                upNextCard.texture = SKTexture(imageNamed: "yellow.png")
            case .Red:
                color = Color.Yellow
                //runAction(SKAction.playSoundFileNamed("3c.mp3", waitForCompletion: false))
                previousCard.texture = SKTexture(imageNamed: "red.png")
                currentCard.texture = SKTexture(imageNamed: "yellow.png")
                upNextCard.texture = SKTexture(imageNamed: "black.png")
            case .Yellow:
                color = Color.Black
                //runAction(SKAction.playSoundFileNamed("3c.mp3", waitForCompletion: false))
                previousCard.texture = SKTexture(imageNamed: "yellow.png")
                currentCard.texture = SKTexture(imageNamed: "black.png")
                upNextCard.texture = SKTexture(imageNamed: "green.png")
            case .Black:
                color = Color.Green
                //runAction(SKAction.playSoundFileNamed("3c.mp3", waitForCompletion: false))
                previousCard.texture = SKTexture(imageNamed: "black.png")
                currentCard.texture = SKTexture(imageNamed: "green.png")
                upNextCard.texture = SKTexture(imageNamed: "purple.png")
            case .Green:
                color = Color.Purple
                //runAction(SKAction.playSoundFileNamed("3c.mp3", waitForCompletion: false))
                previousCard.texture = SKTexture(imageNamed: "green.png")
                currentCard.texture = SKTexture(imageNamed: "purple.png")
                upNextCard.texture = SKTexture(imageNamed: "pink.png")
            case .Purple:
                color = Color.Pink
                //runAction(SKAction.playSoundFileNamed("3c.mp3", waitForCompletion: false))
                previousCard.texture = SKTexture(imageNamed: "purple.png")
                currentCard.texture = SKTexture(imageNamed: "pink.png")
                upNextCard.texture = SKTexture(imageNamed: "brown.png")
            case .Pink:
                color = Color.Brown
                //runAction(SKAction.playSoundFileNamed("3c.mp3", waitForCompletion: false))
                previousCard.texture = SKTexture(imageNamed: "pink.png")
                currentCard.texture = SKTexture(imageNamed: "brown.png")
                upNextCard.texture = SKTexture(imageNamed: "white.png")
            case .Brown:
                color = Color.White
                //runAction(SKAction.playSoundFileNamed("3c.mp3", waitForCompletion: false))
                previousCard.texture = SKTexture(imageNamed: "brown.png")
                currentCard.texture = SKTexture(imageNamed: "white.png")
                upNextCard.texture = SKTexture(imageNamed: "orange.png")
            case .White:
                color = Color.Orange
                //runAction(SKAction.playSoundFileNamed("3c.mp3", waitForCompletion: false))
                previousCard.texture = SKTexture(imageNamed: "white.png")
                currentCard.texture = SKTexture(imageNamed: "orange.png")
                upNextCard.texture = SKTexture(imageNamed: "blue.png")
                
            default:break
            }
        }
    // function for rendering new image and sound when card is flipped
    func flipcolorCard() {
        runAction(SKAction.playSoundFileNamed("cardflip.mp3", waitForCompletion: false))
        currentCard.name = "card"
        switch color
        {
        case .Blue:
            if cardFlipped == false {
                cardFlipped = true
                currentCard.texture = SKTexture(imageNamed: "blue.png")
                runAction(SKAction.playSoundFileNamed("blue", waitForCompletion: false))
            }
            else {
                cardFlipped = false
                currentCard.texture = SKTexture(imageNamed: "obj-10.png")
                runAction(SKAction.playSoundFileNamed("blue", waitForCompletion: false))
            }
        case .Orange:
            if cardFlipped == false {
                cardFlipped = true
                currentCard.texture = SKTexture(imageNamed: "orange.png")
                runAction(SKAction.playSoundFileNamed("orange", waitForCompletion: false))
            }
            else {
                cardFlipped = false
                currentCard.texture = SKTexture(imageNamed: "obj-15.png")
                runAction(SKAction.playSoundFileNamed("orange", waitForCompletion: false))
                
            }
        case .Red:
            if cardFlipped == false {
                cardFlipped = true
                currentCard.texture = SKTexture(imageNamed: "red.png")
                runAction(SKAction.playSoundFileNamed("red", waitForCompletion: false))
            }
            else {
                cardFlipped = false
                currentCard.texture = SKTexture(imageNamed: "obj-19.png")
                runAction(SKAction.playSoundFileNamed("red", waitForCompletion: false))
            }
        case .Yellow:
            if cardFlipped == false {
                cardFlipped = true
                currentCard.texture = SKTexture(imageNamed: "yellow.png")
                runAction(SKAction.playSoundFileNamed("yellow", waitForCompletion: false))
                
            }
            else {
                cardFlipped = false
                
                currentCard.texture = SKTexture(imageNamed: "obj-2.png")
                runAction(SKAction.playSoundFileNamed("yellow", waitForCompletion: false))
                
            }
        case .Black:
            if cardFlipped == false {
                cardFlipped = true
                currentCard.texture = SKTexture(imageNamed: "black.png")
                runAction(SKAction.playSoundFileNamed("black", waitForCompletion: false))
                
                
            }
            else {
                cardFlipped = false
                cardFlipped = false
                currentCard.texture = SKTexture(imageNamed: "obj-16.png")
                runAction(SKAction.playSoundFileNamed("black", waitForCompletion: false))
                
            }
        case .Green:
            if cardFlipped == false {
                cardFlipped = true
                currentCard.texture = SKTexture(imageNamed: "green.png")
                runAction(SKAction.playSoundFileNamed("green", waitForCompletion: false))
                
                
            }
            else {
                cardFlipped = false
                cardFlipped = false
                currentCard.texture = SKTexture(imageNamed: "obj-6.png")
                runAction(SKAction.playSoundFileNamed("green", waitForCompletion: false))
                
            }
        case .Purple:
            if cardFlipped == false {
                cardFlipped = true
                currentCard.texture = SKTexture(imageNamed: "purple.png")
                runAction(SKAction.playSoundFileNamed("purple", waitForCompletion: false))
                
                
            }
            else {
                cardFlipped = false
                currentCard.texture = SKTexture(imageNamed: "purple_1.png")
                runAction(SKAction.playSoundFileNamed("purple", waitForCompletion: false))
                
            }
        case .Pink:
            if cardFlipped == false {
                cardFlipped = true
                currentCard.texture = SKTexture(imageNamed: "pink.png")
                runAction(SKAction.playSoundFileNamed("pink", waitForCompletion: false))
                
                
            }
            else {
                cardFlipped = false
                currentCard.texture = SKTexture(imageNamed: "obj-21.png")
                runAction(SKAction.playSoundFileNamed("pink", waitForCompletion: false))
                
            }
        case .Brown:
            if cardFlipped == false {
                cardFlipped = true
                currentCard.texture = SKTexture(imageNamed: "brown.png")
                runAction(SKAction.playSoundFileNamed("brown", waitForCompletion: false))
                
            }
            else {
                cardFlipped = false
                currentCard.texture = SKTexture(imageNamed: "obj-8.png")
                runAction(SKAction.playSoundFileNamed("brown", waitForCompletion: false))
            }
        case .White:
            if cardFlipped == false {
                cardFlipped = true
                currentCard.texture = SKTexture(imageNamed: "white.png")
                runAction(SKAction.playSoundFileNamed("white", waitForCompletion: false))
            }
            else {
                cardFlipped = false
                currentCard.texture = SKTexture(imageNamed: "obj-5.png")
                runAction(SKAction.playSoundFileNamed("white", waitForCompletion: false))
            }
        default : break
        }
        }
        
        override func didMoveToView(view: SKView) {
        
        bgImage.position = CGPointMake(self.size.width / 2, self.size.height / 2)
        bgImage.zPosition = 1
        self.addChild(bgImage)
        
        back.position = CGPoint(x: 40,y: 720)
        back.name = "back"
        back.userInteractionEnabled = false
        back.zPosition = 2
        self.addChild(back)
        
        previousCard.position = CGPoint(x: self.frame.size.width * 0.15, y: CGRectGetMidY(self.frame))
        previousCard.zPosition = 3
        previousCard.name = "previousCard"
        self.addChild(previousCard)
        
        currentCard.position = CGPoint(x: CGRectGetMidX(self.frame), y: CGRectGetMidY(self.frame))
        currentCard.name = "card"
        currentCard.zPosition = 3
        self.addChild(currentCard)
        
        currentCardBackground.position = CGPoint(x: CGRectGetMidX(self.frame), y: CGRectGetMidY(self.frame))
        currentCardBackground.zPosition = 2
        currentCardBackground.name = "cardBackground"
        self.addChild(currentCardBackground)
        
        upNextCard.position = CGPoint(x: self.frame.size.width * 0.85, y: CGRectGetMidY(self.frame))
        upNextCard.name = "upNextCard"
        upNextCard.zPosition = 3
        self.addChild(upNextCard)
        let shrink = SKAction.scaleBy(0.6, duration: 0)
        upNextCard.runAction(SKAction.scaleBy(0.3, duration: 0))
        
        previousCard.runAction(SKAction.scaleBy(0.3, duration: 0))
        currentCard.runAction(shrink)
        forwardArrow.position = CGPoint(x: self.frame.size.width * 0.6, y: self.frame.size.height * 0.1)
        forwardArrow.zPosition = 2
        forwardArrow.name = "Forward"
        self.addChild(forwardArrow)
        
        backwardArrow.position = CGPoint(x: self.frame.size.width * 0.4, y: self.frame.size.height * 0.1)
        backwardArrow.zPosition = 2
        backwardArrow.name = "Backward"
        self.addChild(backwardArrow)
        
        
        previousCard.hidden = true
        backwardArrow.hidden = true
    }
      //actions performed when nodes are touched on screen
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        for touch in touches {
            let location = touch.locationInNode(self)
            let touchedNode = self.nodeAtPoint(location)
            
            if let name = touchedNode.name {
                if name == "back" {
                    print("in back touch")
                    let transition = SKTransition.crossFadeWithDuration(1.0)
                    let backScene = GameScene(size: self.size)
                    self.view!.presentScene(backScene, transition: transition)
                }
                
                if name == "card" || name == "cardBackground" {
                    flipcolorCard()
                }
                
                if name == "Forward" || name == "upNextCard"{
                    addColor()
                }
                
                if name == "Backward" || name == "previousCard"{
                    subtractColor()
                }
            }
        }
        
    }

}