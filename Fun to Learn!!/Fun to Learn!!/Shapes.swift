//
//  Shapes.swift
//  Fun to Learn!!
//
//  Created by Sankara,Ramya Virajitha on 4/15/16.
//  Copyright © 2016 Sankara,Ramya Virajitha. All rights reserved.
//

import SpriteKit
//Renders logic for Shapes scene
class Shapes: SKScene {
    
    let bgImage = SKSpriteNode(imageNamed: "bg-intro.png")
    let back = SKSpriteNode(imageNamed: "bt_Back.png")
    enum Shape {
        case Circle
        case Triangle
        case Square
        case Rectangle
        case Pentagon
        case Hexagon
        case Octagon
        case Heart
        case Star
        case Diamond
        
    }
    var shape = Shape.Circle
    
    let currentCardBackground = SKSpriteNode(imageNamed: "card1.png")
    let currentCard = SKSpriteNode(imageNamed: "circle.png")
    let upNextCardBackground = SKSpriteNode(imageNamed: "")
    let upNextCard = SKSpriteNode(imageNamed: "triangle.png")
    let previousCard = SKSpriteNode(imageNamed: "diamond.png")
    
    let forwardArrow = SKSpriteNode(imageNamed: "bt_Arrow_Forward.png")
    let backwardArrow = SKSpriteNode(imageNamed: "bt_Arrow_Backward.png")
    var cardFlipped = false
    //function for removing image from scene
    func subtractShape() {
        cardFlipped = false
        forwardArrow.name = "Forward"
        backwardArrow.name = "Backward"
        upNextCard.name = "upNextCard"
        previousCard.name = "previousCard"

        switch shape {
        case .Circle:
            shape =  Shape.Triangle
            
            upNextCard.texture = SKTexture(imageNamed: "square.png")
            currentCard.texture = SKTexture(imageNamed: "triangle.png")
            previousCard.texture = SKTexture(imageNamed: "circle.png")
            previousCard.hidden = true
            backwardArrow.hidden = true
            
        case .Triangle:
            shape = Shape.Square
            //runAction(SKAction.playSoundFileNamed("2c.mp3", waitForCompletion: false))
            upNextCard.texture = SKTexture(imageNamed: "rectangle.png")
            currentCard.texture = SKTexture(imageNamed: "square.png")
            previousCard.texture = SKTexture(imageNamed: "triangle.png")
        case .Square:
            
            shape = Shape.Rectangle
            //runAction(SKAction.playSoundFileNamed("3c.mp3", waitForCompletion: false))
            upNextCard.texture = SKTexture(imageNamed: "pentagon.png")
            currentCard.texture = SKTexture(imageNamed: "rectangle.png")
            previousCard.texture = SKTexture(imageNamed: "square.png")
        case .Rectangle:
            shape = Shape.Pentagon
            //runAction(SKAction.playSoundFileNamed("3c.mp3", waitForCompletion: false))
            upNextCard.texture = SKTexture(imageNamed: "hexagon.png")
            currentCard.texture = SKTexture(imageNamed: "pentagon.png")
            previousCard.texture = SKTexture(imageNamed: "rectangle.png")
        case .Pentagon:
            shape = Shape.Hexagon
            //runAction(SKAction.playSoundFileNamed("3c.mp3", waitForCompletion: false))
            upNextCard.texture = SKTexture(imageNamed: "octagon.png")
            currentCard.texture = SKTexture(imageNamed: "hexagon.png")
            previousCard.texture = SKTexture(imageNamed: "pentagon.png")
        case .Hexagon:
            shape = Shape.Octagon
            //runAction(SKAction.playSoundFileNamed("3c.mp3", waitForCompletion: false))
            upNextCard.texture = SKTexture(imageNamed: "heart.png")
            currentCard.texture = SKTexture(imageNamed: "octagon.png")
            previousCard.texture = SKTexture(imageNamed: "hexagon.png")
        case .Octagon:
            shape = Shape.Heart
            //runAction(SKAction.playSoundFileNamed("3c.mp3", waitForCompletion: false))
            upNextCard.texture = SKTexture(imageNamed: "star.png")
            currentCard.texture = SKTexture(imageNamed: "heart.png")
            previousCard.texture = SKTexture(imageNamed: "octagon.png")
        case .Heart:
            shape = Shape.Star
            //runAction(SKAction.playSoundFileNamed("3c.mp3", waitForCompletion: false))
            upNextCard.texture = SKTexture(imageNamed: "diamond.png")
            currentCard.texture = SKTexture(imageNamed: "star.png")
            previousCard.texture = SKTexture(imageNamed: "heart.png")
        case .Star:
            shape = Shape.Diamond
            //runAction(SKAction.playSoundFileNamed("3c.mp3", waitForCompletion: false))
            upNextCard.texture = SKTexture(imageNamed: "circle.png")
            currentCard.texture = SKTexture(imageNamed: "diamond.png")
            previousCard.texture = SKTexture(imageNamed: "star.png")
        default: break
        }
    }
    //function for adding image from scene
    func addShape() {
        cardFlipped = false
        forwardArrow.name = "Forward"
        backwardArrow.name = "Backward"
        upNextCard.name = "upNextCard"
        previousCard.name = "previousCard"
        switch shape {
        
        case .Circle:
            shape = Shape.Triangle
            //runAction(SKAction.playSoundFileNamed("2c.mp3", waitForCompletion: false))
            previousCard.texture = SKTexture(imageNamed: "diamond.png")
            currentCard.texture = SKTexture(imageNamed: "circle.png")
            upNextCard.texture = SKTexture(imageNamed: "triangle.png")
            previousCard.hidden = false
            backwardArrow.hidden = false
        case .Triangle:
            shape = Shape.Square
            //runAction(SKAction.playSoundFileNamed("3c.mp3", waitForCompletion: false))
            previousCard.texture = SKTexture(imageNamed: "triangle.png")
            currentCard.texture = SKTexture(imageNamed: "square.png")
            upNextCard.texture = SKTexture(imageNamed: "rectangle.png")
        case .Square:
            shape = Shape.Rectangle
            //runAction(SKAction.playSoundFileNamed("3c.mp3", waitForCompletion: false))
            previousCard.texture = SKTexture(imageNamed: "square.png")
            currentCard.texture = SKTexture(imageNamed: "rectangle.png")
            upNextCard.texture = SKTexture(imageNamed: "pentagon.png")
        case .Rectangle:
            shape = Shape.Pentagon
            //runAction(SKAction.playSoundFileNamed("3c.mp3", waitForCompletion: false))
            previousCard.texture = SKTexture(imageNamed: "rectangle.png")
            currentCard.texture = SKTexture(imageNamed: "pentagon.png")
            upNextCard.texture = SKTexture(imageNamed: "hexagon.png")
        case .Pentagon:
            shape = Shape.Hexagon
            //runAction(SKAction.playSoundFileNamed("3c.mp3", waitForCompletion: false))
            previousCard.texture = SKTexture(imageNamed: "pentagon.png")
            currentCard.texture = SKTexture(imageNamed: "hexagon.png")
            upNextCard.texture = SKTexture(imageNamed: "octagon.png")
        case .Hexagon:
            shape = Shape.Octagon
            //runAction(SKAction.playSoundFileNamed("3c.mp3", waitForCompletion: false))
            previousCard.texture = SKTexture(imageNamed: "hexagon.png")
            currentCard.texture = SKTexture(imageNamed: "octagon.png")
            upNextCard.texture = SKTexture(imageNamed: "heart.png")
        case .Octagon:
            shape = Shape.Heart
            //runAction(SKAction.playSoundFileNamed("3c.mp3", waitForCompletion: false))
            previousCard.texture = SKTexture(imageNamed: "octagon.png")
            currentCard.texture = SKTexture(imageNamed: "heart.png")
            upNextCard.texture = SKTexture(imageNamed: "star.png")
        case .Heart:
            shape = Shape.Star
            //runAction(SKAction.playSoundFileNamed("3c.mp3", waitForCompletion: false))
            previousCard.texture = SKTexture(imageNamed: "heart.png")
            currentCard.texture = SKTexture(imageNamed: "star.png")
            upNextCard.texture = SKTexture(imageNamed: "diamond.png")
        case .Star:
            shape = Shape.Diamond
            //runAction(SKAction.playSoundFileNamed("3c.mp3", waitForCompletion: false))
            previousCard.texture = SKTexture(imageNamed: "star.png")
            currentCard.texture = SKTexture(imageNamed: "diamond.png")
            upNextCard.texture = SKTexture(imageNamed: "circle.png")
        case .Diamond:
            shape = Shape.Circle
            //runAction(SKAction.playSoundFileNamed("3c.mp3", waitForCompletion: false))
            previousCard.texture = SKTexture(imageNamed: "diamond.png")
            currentCard.texture = SKTexture(imageNamed: "circle.png")
            upNextCard.texture = SKTexture(imageNamed: "triangle.png")
        default: break
        }
    }
    // function for rendering new image and sound when card is flipped
    func flipshapeCard() {
        runAction(SKAction.playSoundFileNamed("cardflip.mp3", waitForCompletion: false))
        currentCard.name = "card"
        
        switch shape {
        case .Circle:
            if cardFlipped == false {
                cardFlipped = true
                currentCard.texture = SKTexture(imageNamed: "circle.png")
                runAction(SKAction.playSoundFileNamed("circle", waitForCompletion: false))
            }
            //            else {
            //                cardFlipped = false
            //                currentCard.texture = SKTexture(imageNamed: "card01_zeroback.png")
            //                runAction(SKAction.playSoundFileNamed("sound_0", waitForCompletion: false))
            //            }
        case .Triangle:
            if cardFlipped == false {
                cardFlipped = true
                currentCard.texture = SKTexture(imageNamed: "triangle.png")
                runAction(SKAction.playSoundFileNamed("triangle", waitForCompletion: false))
            }
            //            else {
            //                cardFlipped = false
            //                currentCard.texture = SKTexture(imageNamed: "card01_oneback.png")
            //                runAction(SKAction.playSoundFileNamed("sound_1", waitForCompletion: false))
            //
            //            }
        case .Square:
            if cardFlipped == false {
                cardFlipped = true
                currentCard.texture = SKTexture(imageNamed: "square.png")
                runAction(SKAction.playSoundFileNamed("square", waitForCompletion: false))
            }
            //            else {
            //                cardFlipped = false
            //                currentCard.texture = SKTexture(imageNamed: "card01_twoback.png")
            //                runAction(SKAction.playSoundFileNamed("sound_2", waitForCompletion: false))
            //            }
        case .Rectangle:
            if cardFlipped == false {
                cardFlipped = true
                currentCard.texture = SKTexture(imageNamed: "rectangle.png")
                runAction(SKAction.playSoundFileNamed("rectangle", waitForCompletion: false))
                
            }
            //            else {
            //                cardFlipped = false
            //
            //                currentCard.texture = SKTexture(imageNamed: "card01_threeback.png")
            //                runAction(SKAction.playSoundFileNamed("sound_3", waitForCompletion: false))
            //
            //            }
        case .Pentagon:
            if cardFlipped == false {
                cardFlipped = true
                currentCard.texture = SKTexture(imageNamed: "pentagon.png")
                runAction(SKAction.playSoundFileNamed("pentagon", waitForCompletion: false))
                
                
            }
            //            else {
            //                cardFlipped = false
            //                cardFlipped = false
            //                currentCard.texture = SKTexture(imageNamed: "card01_fourback.png")
            //                runAction(SKAction.playSoundFileNamed("sound_4", waitForCompletion: false))
            //
            //            }
        case .Hexagon:
            if cardFlipped == false {
                cardFlipped = true
                currentCard.texture = SKTexture(imageNamed: "hexagon.png")
                runAction(SKAction.playSoundFileNamed("hexagon", waitForCompletion: false))
                
                
            }
            //            else {
            //                cardFlipped = false
            //                currentCard.texture = SKTexture(imageNamed: "card01_fiveback.png")
            //                runAction(SKAction.playSoundFileNamed("sound_5", waitForCompletion: false))
            //
            //            }
        case .Octagon:
            if cardFlipped == false {
                cardFlipped = true
                currentCard.texture = SKTexture(imageNamed: "octagon.png")
                runAction(SKAction.playSoundFileNamed("octagon", waitForCompletion: false))
                
                
            }
            //            else {
            //                cardFlipped = false
            //                currentCard.texture = SKTexture(imageNamed: "card01_sixback.png")
            //                runAction(SKAction.playSoundFileNamed("sound_6", waitForCompletion: false))
            //
            //            }
        case .Heart:
            if cardFlipped == false {
                cardFlipped = true
                currentCard.texture = SKTexture(imageNamed: "heart.png")
                runAction(SKAction.playSoundFileNamed("heart", waitForCompletion: false))
                
            }
            //            else {
            //                cardFlipped = false
            //                currentCard.texture = SKTexture(imageNamed: "card01_sevenback.png")
            //                runAction(SKAction.playSoundFileNamed("sound_7", waitForCompletion: false))
            //            }
        case .Star:
            if cardFlipped == false {
                cardFlipped = true
                currentCard.texture = SKTexture(imageNamed: "star.png")
                runAction(SKAction.playSoundFileNamed("star", waitForCompletion: false))
            }
            //            else {
            //                cardFlipped = false
            //                currentCard.texture = SKTexture(imageNamed: "card01_eightback.png")
            //                runAction(SKAction.playSoundFileNamed("sound_8", waitForCompletion: false))
            //            }
        case .Diamond:
            if cardFlipped == false {
                cardFlipped = true
                currentCard.texture = SKTexture(imageNamed: "diamond.png")
                runAction(SKAction.playSoundFileNamed("diamond", waitForCompletion: false))
            }
        default:break
        }
    }
    
    override func didMoveToView(view: SKView) {
        
        bgImage.position = CGPointMake(self.size.width / 2, self.size.height / 2)
        bgImage.zPosition = 1
        self.addChild(bgImage)
        //back button
        back.position = CGPoint(x: 40,y: 720)
        back.name = "back"
        back.userInteractionEnabled = false
        back.zPosition = 2
        self.addChild(back)
        
        previousCard.position = CGPoint(x: self.frame.size.width * 0.15, y: CGRectGetMidY(self.frame))
        previousCard.zPosition = 3
        previousCard.name = "previousCard"
        self.addChild(previousCard)
        
        currentCard.position = CGPoint(x: CGRectGetMidX(self.frame), y: CGRectGetMidY(self.frame))
        currentCard.name = "card"
        currentCard.zPosition = 3
        self.addChild(currentCard)
        
        currentCardBackground.position = CGPoint(x: CGRectGetMidX(self.frame), y: CGRectGetMidY(self.frame))
        currentCardBackground.zPosition = 2
        currentCardBackground.name = "cardBackground"
        self.addChild(currentCardBackground)
        
        upNextCard.position = CGPoint(x: self.frame.size.width * 0.85, y: CGRectGetMidY(self.frame))
        upNextCard.name = "upNextCard"
        upNextCard.zPosition = 3
        self.addChild(upNextCard)
        let shrink = SKAction.scaleBy(0.6, duration: 0)
        upNextCard.runAction(SKAction.scaleBy(0.3, duration: 0))
        
        previousCard.runAction(SKAction.scaleBy(0.3, duration: 0))
        currentCard.runAction(shrink)
        //forward arrow
        forwardArrow.position = CGPoint(x: self.frame.size.width * 0.6, y: self.frame.size.height * 0.1)
        forwardArrow.zPosition = 2
        forwardArrow.name = "Forward"
        self.addChild(forwardArrow)
        //backward arrow
        backwardArrow.position = CGPoint(x: self.frame.size.width * 0.4, y: self.frame.size.height * 0.1)
        backwardArrow.zPosition = 2
        backwardArrow.name = "Backward"
        self.addChild(backwardArrow)
        
        
        previousCard.hidden = true
        backwardArrow.hidden = true
    }
      //actions performed when nodes are touched on screen
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        for touch in touches {
            let location = touch.locationInNode(self)
            let touchedNode = self.nodeAtPoint(location)
            
            if let name = touchedNode.name {
                if name == "back" {
                    print("in back touch")
                    let transition = SKTransition.crossFadeWithDuration(1.0)
                    let backScene = GameScene(size: self.size)
                    self.view!.presentScene(backScene, transition: transition)
                }
                
                if name == "card" || name == "cardBackground" {
                    flipshapeCard()
                }
                
                if name == "Forward" || name == "upNextCard"{
                    addShape()
                }
                
                if name == "Backward" || name == "previousCard"{
                    subtractShape()
                }
            }
        }
        
    }
    
}

