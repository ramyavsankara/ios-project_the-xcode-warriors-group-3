//
//  GameViewController.swift
//  Fun to Learn!!
//
//  Created by Sankara,Ramya Virajitha on 3/14/16.
//  Copyright (c) 2016 Sankara,Ramya Virajitha. All rights reserved.
//

import UIKit
import SpriteKit
import AVFoundation

class GameViewController: UIViewController {
    var backgroundMusicPlayer: AVAudioPlayer!

    override func viewDidLoad() {
        super.viewDidLoad()

        if let scene = GameScene(fileNamed:"GameScene") {
            // Configure the view.
            let skView = self.view as! SKView
            
            skView.showsFPS = true
            skView.showsNodeCount = true
            
            /* Sprite Kit applies additional optimizations to improve rendering performance */
            skView.ignoresSiblingOrder = true
            
            /* Set the scale mode to scale to fit the window */
            scene.scaleMode = .AspectFill
            
            skView.presentScene(scene)
           // playBackgroundMusic("background.mp3")
        }
    }

    override func shouldAutorotate() -> Bool {
        return true
    }

    override func supportedInterfaceOrientations() -> UIInterfaceOrientationMask {
        if UIDevice.currentDevice().userInterfaceIdiom == .Phone {
            return .AllButUpsideDown
        } else {
            return .All
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Release any cached data, images, etc that aren't in use.
    }

    override func prefersStatusBarHidden() -> Bool {
        return true
    }
    
//    func playBackgroundMusic(filename: String) {
//        let url = NSBundle.mainBundle().URLForResource(
//            filename, withExtension: nil)
//        if (url == nil) {
//            print("Could not find file: \(filename)")
//            return
//        }
//        
//        var error: NSError? = nil
//        do {
//            backgroundMusicPlayer =
//                try AVAudioPlayer(contentsOfURL: url!)
//        } catch let error1 as NSError {
//            error = error1
//            backgroundMusicPlayer = nil
//        }
//        if backgroundMusicPlayer == nil {
//            print("Could not create audio player: \(error!)")
//            return
//        }
//        
//        backgroundMusicPlayer.numberOfLoops = -1
//        backgroundMusicPlayer.prepareToPlay()
//        backgroundMusicPlayer.play()
//    }
}
